#!/bin/bash

# write_pbins_config
write_pbins_config() {
    local config_dir=${1:-/etc/paludis}
    cat <<EOF > ${config_dir}/repositories/pbins.conf
location = /var/db/paludis/repositories/pbins-${TARGET}
format = e
importance = -100
binary_destination = true
binary_distdir = /var/db/paludis/repositories/pbins-${TARGET}/distfiles
distdir = /var/db/paludis/repositories/pbins-${TARGET}/distfiles
binary_keywords_filter = ~${PLATFORM} ${PLATFORM}
binary_uri_prefix = mirror://exherbo-pbins/${TARGET}/
EOF

    mkdir -p /var/db/paludis/repositories/pbins-${TARGET}/{distfiles,metadata,packages,profiles}

    mkdir -p /var/cache/paludis/pbins-distfiles
    mkdir -p /var/db/paludis/repositories/pbins-${TARGET}/distfiles
    chown paludisbuild:paludisbuild /var/cache/paludis/pbins-distfiles
    chown paludisbuild:paludisbuild /var/db/paludis/repositories/pbins-${TARGET}/distfiles
    chmod g+w /var/cache/paludis/pbins-distfiles
    chmod g+w /var/db/paludis/repositories/pbins-${TARGET}/distfiles

    echo pbins-${TARGET} > /var/db/paludis/repositories/pbins-${TARGET}/profiles/repo_name
    : > /var/db/paludis/repositories/pbins-${TARGET}/metadata/categories.conf
    echo masters = arbor > /var/db/paludis/repositories/pbins-${TARGET}/metadata/layout.conf
}
