CPN_SKIP_TESTS+=(
    # Fails a test
    # Last checked: 08 Sep 2020 (version: 0.13.0)
    dev-libs/check

    # the `more` command hangs
    # Last checked 09 Feb 2023 (version: 5.2.1)
    sys-apps/gawk
)
