CPN_SKIP_TESTS+=(
    # Fails many tests when updating from 3.5.19 (because its using the old system lib?)
    # Last checked: 15 Aug 2019 (version: 3.6.8)
    # Context:
    #
    # ============================================================================
    # Testsuite summary for GnuTLS 3.6.8
    # ============================================================================
    # # TOTAL: 365
    # # PASS:  13
    # # SKIP:  8
    # # XFAIL: 0
    # # FAIL:  344
    # # XPASS: 0
    # # ERROR: 0
    #
    dev-libs/gnutls

    # Testsuite aborts with error
    # Last checked: 13 Sep 2019 (version 6.0-r5)
    # Context:
    # #####  testing unzip -o and funzip (ignore funzip warning)
    # funzip warning: zipfile has more than one entry--rest ignored
    # error: invalid zip file with overlapped components (possible zip bomb)
    # make: *** [Makefile:501: check] Error 12
    app-arch/unzip

    # Fails two tests
    # Last checked: 07 May 2020 (version 1.0.7)
    # Context:
    # FAIL: t0004-core-chdir.sh
    # FAIL: t0003-core-basic.sh
    sys-apps/sydbox

    # Fails the test-strerror_r test
    # Last checked: 24 Oct 2020 (version: 0.21-r1)
    sys-devel/gettext

    # Fails the j_recover_fast_commit test
    # Last checked: 01 May 2021 (version 1.46.2)
    sys-fs/e2fsprogs

    # Fails a test
    # Last checked: 07 Dec 2021 (version: 1.34)
    # Context:
    #
    # --- /dev/null   2021-12-03 11:40:16.613832588 +0000
    # +++ /var/tmp/paludis/build/app-arch-tar-1.34/work/tar-1.34/tests/testsuite.dir/at-groups/151/stderr     2021-12-07 06:31:24.329834032 +0000
    # @@ -0,0 +1,363 @@
    # +tar: dir/f@-9223372036854775808: Cannot stat: Value too large for defined data type
    # +tar: dir/f@-9223372036854775807: Cannot stat: Value too large for defined data type
    # +tar: dir/f@-9223372036854775807.: Cannot stat: Value too large for defined data type
    # +tar: dir/f@-9223372036854775807.01: Cannot stat: Value too large for defined data type
    # +tar: dir/f@-9223372036854775807.001: Cannot stat: Value too large for defined data type
    # [...]
    # +tar: Exiting with failure status due to previous errors
    # 151. time01.at:20: 151. time: tricky time stamps (time01.at:20): FAILED (time01.at:23)
    #
    # Bug-Report: https://www.mail-archive.com/bug-tar@gnu.org/msg06038.html
    app-arch/tar

    # Fails two tests
    # Last checked: 20 Sep 2022 (version: 4.8)
    # Context:
    # ../build-aux/test-driver: line 112: 1786399 Aborted                 (core dumped) "$@" >> "$log_file" 2>&1
    # FAIL: test-perror2
    # ../build-aux/test-driver: line 112: 1786682 Aborted                 (core dumped) "$@" >> "$log_file" 2>&1
    # FAIL: test-strerror_r
    sys-apps/sed

    # Fails some tests
    # Last checked: 10 Nov 2022 (version: 1.6.3)
    # Context:
    # ERROR: in testcase ./testsuite/runtest.libs/libs.exp
    # ERROR:  send: spawn id exp7 not open
    # ERROR:  tcl error code NONE
    #
    # dejagnu's config.guess wrongly detects the host as
    # aarch64-unknown-linux-gnu when an aarch64 runner is used to build the
    # armv7 stage:
    # Native configuration is aarch64-unknown-linux-gnu
    #
    # So we probably have to fix that first
    dev-util/dejagnu

    # the `more` command hangs
    # Last checked 09 Feb 2023 (version: 5.2.1)
    sys-apps/gawk
)
